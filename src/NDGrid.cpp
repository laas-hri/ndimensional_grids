#include "ndgrid/NDGrid.hpp"
#include "ndgrid/NDGridAlgo.hpp"

namespace ndimensional_grids
{
template class nDimGrid<float, 2>;
template class nDimGrid<float, 3>;
template class nDimGrid<double, 2>;
template class nDimGrid<double, 3>;

} // namespace ndimensional_grids
