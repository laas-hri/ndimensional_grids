# ndimensional_grids

library for manipulating ndimensional grids

## Dependencies
- Eigen3

## Classes

**NDGrid**
- nDimGrid

**NDGridAlgo**
- DijkstraCell
- Dijkstra


## Unit testing
- nDimGrid
- DijkstraCell
