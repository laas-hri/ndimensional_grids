#include <limits.h>
#include "ndgrid/NDGrid.hpp"
#include "ndgrid/NDGridAlgo.hpp"
#include "gtest/gtest.h"

#include <iostream>

class GridTests : public ::testing::Test {
 protected:
  virtual void SetUp() {
  }

  virtual void TearDown() {
    // Code here will be called immediately after each test
    // (right before the destructor).
  }
};


TEST_F(GridTests, TestsGrid1)
{
  // Declarations
  std::array<int, 4> ArrayC={1,4,2,5};
  std::array<float,2> cellSize={1.0,1.0};
  ndimensional_grids::nDimGrid<float,2> Grid(cellSize, false ,{1,4,2,5});
  

  std::array<float,2> corner={1,2};
  std::array<size_t,2> shapeRes={3,3};
  std::array<size_t,2> cellCoord1={2,0};
  std::array<size_t,2> cellCoord2={2,2};
  std::array<float,2> cellCenter1={1.5,2.5};
  std::array<float,2> cellCenter2={2.5,3.5};
  std::vector<float> values(9,0); 
  
  // Operations
  ASSERT_NO_THROW((ndimensional_grids::nDimGrid<float,2> (cellSize, false ,{1,4,2,5})));
  ASSERT_THROW((ndimensional_grids::nDimGrid<float,2>(cellSize, false ,{1.0,4.0})), std::invalid_argument);
  ASSERT_EQ(Grid.getNumberOfCells(),9);
  ASSERT_EQ(Grid.neighboursNumber(),8);

  ASSERT_EQ(Grid.getCellSize(),cellSize);
  ASSERT_EQ(Grid.getOriginCorner(),corner);
  ASSERT_EQ(Grid.shape(), shapeRes);
  ASSERT_EQ(Grid.getCellCoord(2), cellCoord1);
  ASSERT_EQ(Grid.getCellCoord(8), cellCoord2);

  ASSERT_EQ(Grid.getCellCenter({0,0}), cellCenter1);
  ASSERT_EQ(Grid.getCellCenter({1,1}), cellCenter2);

  ASSERT_EQ(Grid.getCell(0), 0);
  ASSERT_EQ(Grid.getCell(5), 0);
  ASSERT_EQ(Grid.getValues(), values);
}


TEST_F(GridTests, TestsGrid2)
{
  // Declarations
  ndimensional_grids::nDimGrid<float,1> Grid(0.5, false, {1,6});
  std::array<float,1> cellSize={0.5};

  std::array<float,1> corner={1};
  std::array<size_t,1> shapeRes={10};
  std::array<size_t,1> cellCoord1={2};
  std::array<size_t,1> cellCoord2={8};
  std::array<float,1> cellCenter1={1.25};
  std::array<float,1> cellCenter2={1.75};
  std::vector<float> values(10,0); 


  // Operations
  ASSERT_NO_THROW((ndimensional_grids::nDimGrid<float,1> (cellSize, false, {1,6})));
  ASSERT_THROW((ndimensional_grids::nDimGrid<float,1>(cellSize, false ,{1})), std::invalid_argument);
  ASSERT_EQ(Grid.getNumberOfCells(),10);
  ASSERT_EQ(Grid.neighboursNumber(),2);

  ASSERT_EQ(Grid.getCellSize(),cellSize);
  ASSERT_EQ(Grid.getOriginCorner(),corner);
  ASSERT_EQ(Grid.shape(), shapeRes);
  ASSERT_EQ(Grid.getCellCoord(2), cellCoord1);
  ASSERT_EQ(Grid.getCellCoord(8), cellCoord2);

  ASSERT_EQ(Grid.getCellCenter({0}), cellCenter1);
  ASSERT_EQ(Grid.getCellCenter({1}), cellCenter2);

  ASSERT_EQ(Grid.getCell(0), 0);
  ASSERT_EQ(Grid.getCell(5), 0);
  ASSERT_EQ(Grid.getValues(), values);
}


TEST_F(GridTests, TestsGrid3)
{
  // Declarations
  std::array<size_t, 2> ArrayC={10,10};
  std::array<float,2> cellSize={2.0,2.0};
  std::array<float,2> Origin={2.0,2.0};
  ndimensional_grids::nDimGrid<float,2> Grid(Origin, ArrayC, cellSize);
  

  std::array<float,2> corner={2.0,2.0};
  std::array<size_t,2> shapeRes={10,10};
  std::array<size_t,2> cellCoord1={2,0};
  std::array<size_t,2> cellCoord2={8,0};
  std::array<float,2> cellCenter1={3,3};
  std::array<float,2> cellCenter2={5,5};
  std::vector<float> values(100,0); // 100 times 0
  
  // Operations
  ASSERT_EQ(Grid.getNumberOfCells(),100);
  ASSERT_EQ(Grid.neighboursNumber(),8);

  ASSERT_EQ(Grid.getCellSize(),cellSize);
  ASSERT_EQ(Grid.getOriginCorner(),corner);
  ASSERT_EQ(Grid.shape(), shapeRes);
  ASSERT_EQ(Grid.getCellCoord(2), cellCoord1);
  ASSERT_EQ(Grid.getCellCoord(8), cellCoord2);

  ASSERT_EQ(Grid.getCellCenter({0,0}), cellCenter1);
  ASSERT_EQ(Grid.getCellCenter({1,1}), cellCenter2);

  ASSERT_EQ(Grid.getCell(0), 0);
  ASSERT_EQ(Grid.getCell(5), 0);
  ASSERT_EQ(Grid.getValues(), values);
}


TEST_F(GridTests, TestsDijkstraCell)
{
  // Declarations
  ndimensional_grids::DijkstraCell<float,float> Cell1;
  ndimensional_grids::DijkstraCell<float,float> Cell2;

  // Operations
  Cell1.dist=5;
  Cell1.cell=6;

  Cell2.dist=9;
  Cell2.cell=15;
  ASSERT_FALSE(Cell1>Cell2);
  ASSERT_FALSE(Cell1==Cell2);
  ASSERT_TRUE(Cell1!=Cell2);

  Cell1.cell=15;
  ASSERT_TRUE(Cell1==Cell2);
  ASSERT_FALSE(Cell2!=Cell1);
  ASSERT_FALSE(Cell1>Cell2);

  Cell1.dist=25;
  ASSERT_TRUE(Cell1>Cell2);
}


TEST_F(GridTests, TestsDijkstra)
{
  // Declarations
  ndimensional_grids::Dijkstra<ndimensional_grids::nDimGrid<bool,2>,float> distGrid_r;
  ndimensional_grids::nDimGrid<bool,2> freespace_r;

  float max_dist=50;

  //ndimensional_grids::nDimGrid<ndimensional_grids::DijkstraCell<bool,2>,2> GridRes;
  std::vector<bool> freeSpaceValues(9, false);
  std::array<float,2> freeSpaceCorner={1.0,1.0};
  std::array<size_t,2> shapeRes={3,3};

  // Operations
  //fromr[0]=3.5;
  //fromr[1]=1.5;
  freespace_r=ndimensional_grids::nDimGrid<bool,2>({1.0,1.0}, false ,{1,4,1,4});
  distGrid_r = ndimensional_grids::Dijkstra<ndimensional_grids::nDimGrid<bool,2>,float>(&freespace_r, freespace_r.getCellCoord(2), max_dist);

  ASSERT_EQ(freespace_r.getNumberOfCells(),9);
  ASSERT_EQ(freespace_r.neighboursNumber(),8);
  ASSERT_EQ(freespace_r.getValues(), freeSpaceValues);
  ASSERT_EQ(freespace_r.getOriginCorner(),freeSpaceCorner);
  ASSERT_EQ(freespace_r.shape(), shapeRes);

  ASSERT_EQ(distGrid_r.getCost(2),0);
  ASSERT_EQ(distGrid_r.getCost({2,0}),0.0);
  ASSERT_EQ(distGrid_r.getCostPos({3.2,1.65}),0.0);
  /*ASSERT_EQ(distGrid_r.getCost({2,1}),0); // inf
  ASSERT_EQ(distGrid_r.getCost({0,0}),0);
  ASSERT_EQ(distGrid_r.getCost(1),0);
  ASSERT_EQ(distGrid_r.getCost(3),0);
  ASSERT_EQ(distGrid_r.getCost(4),0);
  ASSERT_EQ(distGrid_r.getCost(5),0);
  ASSERT_EQ(distGrid_r.getCost(6),0);
  ASSERT_EQ(distGrid_r.getCost(7),0);
  ASSERT_EQ(distGrid_r.getCost(8),0);*/
}


int main(int argc, char **argv) 
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
